import argparse
import glob
import os
import os.path as osp
import time

import cv2
import torch
import torchvision
from termcolor import cprint

from train import get_object_detection_model


def output_empty_text(path: str):
    with open(path, "w") as f:
        f.write("\n")


def detect_one_frame(frame, frame_idx, device, model, output_video=None):
    start = time.time()
    image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    image_tensor = torchvision.transforms.functional.to_tensor(image)
    image_tensor = image_tensor.to(device)

    output = model([image_tensor])

    boxes = output[0]["boxes"].cpu().detach().numpy()
    labels = output[0]["labels"].cpu().detach().numpy()
    scores = output[0]["scores"].cpu().detach().numpy()

    result_image = image.copy()
    num_objects = boxes.shape[0]
    color = (255, 0, 0)  # RGB

    print("-" * 60)
    for i in range(num_objects):
        if scores[i] > args.confidence:
            print(f"Score: {int(scores[i]*100):03}% Label: {labels[i]}")
            result_image = cv2.rectangle(
                result_image,
                (boxes[i, 0], boxes[i, 1]),
                (boxes[i, 2], boxes[i, 3]),
                color=color,
                thickness=2,
            )

            if args.output_text_dir is not None:
                with open(
                    osp.join(args.output_text_dir, f"{frame_idx:>06}.txt"),
                    mode="a",
                ) as f:

                    if args.output_text_format == "det":
                        # class_index x1 y1 x2 y2 confidence
                        f.write(
                            f"{1} {boxes[i, 0]} "
                            + f"{boxes[i, 1]} {boxes[i, 2]} "
                            + f"{boxes[i, 3]} {scores[i]}\n"
                        )
                    elif args.output_text_format == "mot":
                        # frame_index track_id left top width height confidence
                        f.write(
                            f"{frame_idx} -1 {boxes[i, 0]} "
                            + f"{boxes[i, 1]} {boxes[i, 2] - boxes[i, 0]} "
                            + f"{boxes[i, 3] - boxes[i, 1]} {scores[i]}\n"
                        )

    # output empty file if no objects (higher than confidence) is detected
    if sum(scores > args.confidence) == 0 and args.output_text_dir is not None:
        print("no obj")
        with open(
            osp.join(args.output_text_dir, f"{frame_idx:>06}.txt"), mode="a"
        ) as f:
            f.write("\n")

    result_image = cv2.cvtColor(result_image, cv2.COLOR_RGB2BGR)
    if output_video is not None:
        output_video.write(result_image)
    if not args.no_animation:
        cv2.imshow("", cv2.resize(result_image, (1024, 768)))

    frame_idx += 1
    print(f"FPS: {1 / (time.time() - start): 02.2f}")


def main(args):
    device = "cuda" if torch.cuda.is_available() else "cpu"

    model = get_object_detection_model(num_classes=2)
    model.load_state_dict(torch.load(args.model_path))
    model = model.to(device)
    model.eval()

    if args.output_text_dir is not None:
        os.makedirs(args.output_text_dir, exist_ok=True)

    if args.output_video_path is not None:
        fourcc = cv2.VideoWriter_fourcc(*"XVID")
        output_video = cv2.VideoWriter(
            args.output_video_path, fourcc, args.output_video_fps, (320, 240)
        )

    frame_idx = 1
    if osp.isdir(args.input):
        images_path = sorted(glob.glob(osp.join(args.input, "*")))
        for image_path in images_path:
            frame = cv2.imread(image_path)
            if args.output_video_path is not None:
                detect_one_frame(frame, frame_idx, device, model, output_video)
            else:
                detect_one_frame(frame, frame_idx, device, model)
            key = cv2.waitKey(1)
            if key & 0xFF == ord("q"):
                break
            frame_idx += 1
    else:
        capture = cv2.VideoCapture(args.input)
        assert capture.isOpened(), "Cannot detect capture device"

        while capture.isOpened():
            ret, frame = capture.read()
            if ret:
                if args.output_video_path is not None:
                    detect_one_frame(
                        frame, frame_idx, device, model, output_video
                    )
                else:
                    detect_one_frame(frame, frame_idx, device, model)
                key = cv2.waitKey(1)
                if key & 0xFF == ord("q"):
                    break
                frame_idx += 1
            else:
                break
        capture.release()

    if args.output_video_path is not None:
        output_video.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    cprint("-" * 50, "yellow")
    cprint(("Faster R-CNN Demo").center(50), "yellow")
    cprint("-" * 50, "yellow")

    parser = argparse.ArgumentParser(
        description="Faster R-CNN demo with capture"
    )
    parser.add_argument(
        "--input",
        help="Device number, device path, video file path or images dir",
        default=0,
    )
    parser.add_argument(
        "--model-path", help="Path to .weights-file", default=0
    )
    parser.add_argument(
        "--confidence",
        help="Confidence to filter predictions",
        type=float,
        default=0.7,
    )
    parser.add_argument(
        "--output-text-dir",
        help="If output the detected bbox, path to the directory",
        default=None,
    )
    parser.add_argument(
        "--output-text-format",
        help="If output the detected bbox, format of the text",
        default="mot",
        choices=["det", "mot"],
    )
    parser.add_argument(
        "--output-video-path",
        help="If output the detected video, path to the video",
        default=None,
    )
    parser.add_argument(
        "--output-video-fps",
        help="If output the detected video, value of fps of the video",
        type=int,
        default=30,
    )
    parser.add_argument(
        "--no-animation",
        action="store_true",
        help="Do not show video on display",
    )
    args = parser.parse_args()

    main(args)
