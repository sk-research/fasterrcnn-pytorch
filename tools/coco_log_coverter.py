import argparse
import re
import warnings

import numpy as np
import pandas as pd
import pytablewriter

warnings.filterwarnings("ignore")


def main(args):
    metrics = np.loadtxt(args.file)
    metrics_df = pd.DataFrame(
        data={
            "metric": "AP",
            "IoU": [
                "0.50:0.95",
                "0.5",
                "0.75",
                "0.50:0.95",
                "0.50:0.95",
                "0.50:0.95",
            ],
            "area": ["all", "all", "all", "all", "all", "all"],
            "maxDets": [100, 100, 100, 100, 100, 100],
            "value": metrics[:6],
        }
    )

    if args.format == "latex":
        print(
            re.sub(r"\\.*rule", r"\\hline", metrics_df.to_latex(index=False))
        )

    elif args.format in ("markdown", "md"):
        writer = pytablewriter.MarkdownTableWriter()
        writer.from_dataframe(metrics_df)
        writer.write_table()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert COCO log text to tex table format"
    )
    parser.add_argument(
        "-f", "--file", help="Path to log file", type=str, required=True
    )
    parser.add_argument(
        "--format",
        help="Format to convert",
        choices=["latex", "markdown", "md"],
        type=str,
        default="latex",
    )
    args = parser.parse_args()

    main(args)
