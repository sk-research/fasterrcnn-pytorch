import argparse
import glob
import json
import os.path as osp

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set_context("poster")


def analyze_coco(txt_dir: str, output_path: str):
    """Analyze with COCO metrics.
    Refernce: http://cocodataset.org/#detection-eval
    """
    log_files = sorted(glob.glob(osp.join(txt_dir, "*.txt")))

    metrics = np.array([])

    for log_file in log_files:
        metric = np.loadtxt(log_file)
        metrics = np.append(metrics, metric)

    metrics = metrics.reshape((-1, 12))  # 12 metric type

    x = list(range(1, metrics.shape[0] + 1))

    plt.figure(figsize=(18, 12))
    plt.rcParams["font.size"] = 24
    plt.plot(x, metrics[:, 0], "-", label="ap_iou_0.5_0.95")
    plt.plot(x, metrics[:, 1], "-", label="ap_iou_0.5")
    plt.plot(x, metrics[:, 2], "-", label="ap_iou_0.75")
    plt.plot(x, metrics[:, 3], "-", label="ap_area_small")
    plt.plot(x, metrics[:, 4], "-", label="ar_area_midium")
    plt.plot(x, metrics[:, 5], "-", label="ar_area_large")
    plt.title("coco metric")
    plt.xlabel("epoch")
    plt.ylabel("AP: Average Precision")
    plt.legend(loc="upper right")

    plt.savefig(output_path)


def analyze_loss(json_dir: str, output_path: str):
    log_files = sorted(glob.glob(osp.join(json_dir, "*.json")))

    loss_classifier = []
    loss_box_reg = []
    loss_objectness = []
    loss_rpn_box_reg = []
    loss = []

    for log_file in log_files:
        with open(log_file, mode="r") as f:
            log_dict = json.load(f)
            loss_classifier.append(log_dict["loss_classifier"])
            loss_box_reg.append(log_dict["loss_box_reg"])
            loss_objectness.append(log_dict["loss_objectness"])
            loss_rpn_box_reg.append(log_dict["loss_rpn_box_reg"])
            loss.append(sum(log_dict.values()))

    x = list(range(1, len(log_files) + 1))

    plt.figure(figsize=(18, 12))
    plt.rcParams["font.size"] = 24
    plt.plot(x, loss_classifier, "-", label="loss_classifier")
    plt.plot(x, loss_box_reg, "-", label="loss_box_reg")
    plt.plot(x, loss_objectness, "-", label="loss_objectness")
    plt.plot(x, loss_rpn_box_reg, "-", label="loss_rpn_box_reg")
    plt.plot(x, loss, "-", label="loss")
    plt.title("loss")
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.legend(loc="upper right")

    plt.savefig(output_path)


def main(args):
    if args.metric == "coco":
        analyze_coco(txt_dir=args.log_dir, output_path=args.output_path)

    elif args.metric == "loss":
        analyze_loss(json_dir=args.log_dir, output_path=args.output_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Analyze Faster R-CNN training log"
    )
    parser.add_argument(
        "--log-dir", help="Path to directory for logs", type=str, required=True
    )
    parser.add_argument(
        "--metric",
        help="Metric type",
        choices=["coco", "loss"],
        default="coco",
    )
    parser.add_argument(
        "--output-path",
        help="Path to output graph",
        type=str,
        default="log.png",
    )
    args = parser.parse_args()

    main(args)
