import argparse
import glob
import json
import math
import os
import os.path as osp
import sys

from termcolor import cprint
import torch
import torch.utils.data
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from PIL import Image

from evaluate import evaluate
import transforms as T
import utils


def get_object_detection_model(num_classes):
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(
        pretrained=True
    )
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    return model


def get_transform(is_train):
    transforms = []
    transforms.append(T.ToTensor())
    if is_train:
        pass
    return T.Compose(transforms)


class Dataset(torch.utils.data.Dataset):
    def __init__(self, images_dir, labels_dir, height, width, transforms=None):
        self.images_dir = images_dir
        self.labels_dir = labels_dir
        self.height = height
        self.width = width
        self.transforms = transforms

        self.images_path = [
            image_path
            for image_path in sorted(glob.glob(osp.join(self.images_dir, "*")))
        ]

        self.labels = []
        labels_path = sorted(glob.glob(osp.join(labels_dir, "*")))
        for label_path in labels_path:
            with open(label_path, mode="r") as f:
                annos = [
                    list(map(int, anno.split())) for anno in f.readlines()
                ]
                labels = {}
                labels["labels"] = [anno[0] for anno in annos]
                # format: ltrb
                labels["boxes"] = [anno[1:] for anno in annos]
                labels["area"] = [
                    (anno[3] - anno[1]) * (anno[4] - anno[2]) for anno in annos
                ]
                labels["iscrowd"] = len(annos)

                self.labels.append(labels)

    def __getitem__(self, idx):
        image_path = self.images_path[idx]
        image = Image.open(image_path).convert("RGB")
        image = image.resize(
            (self.width, self.height), resample=Image.BILINEAR
        )

        target = {}
        target["image_id"] = torch.Tensor([idx])
        target["labels"] = torch.as_tensor(
            self.labels[idx]["labels"], dtype=torch.int64
        )
        target["boxes"] = torch.as_tensor(
            self.labels[idx]["boxes"], dtype=torch.float32
        )
        target["area"] = torch.as_tensor(
            self.labels[idx]["area"], dtype=torch.int64
        )
        target["iscrowd"] = torch.zeros(
            (self.labels[idx]["iscrowd"],), dtype=torch.int64
        )

        if self.transforms is not None:
            image, target = self.transforms(image, target)

        return image, target

    def __len__(self):
        return len(self.images_path)


def train_one_epoch(
    model,
    optimizer,
    data_loader,
    device,
    epoch,
    num_epochs,
    print_freq,
    log_path,
):
    model.train()
    metric_logger = utils.MetricLogger(delimiter=" ")
    metric_logger.add_meter(
        "lr", utils.SmoothedValue(window_size=1, fmt="{value:.6f}")
    )
    header = f"Train: [{epoch + 1:>{len(str(num_epochs))}}/{num_epochs}]"

    lr_scheduler = None
    if epoch == 0:
        warmup_factor = 1.0 / 1000
        warmup_iters = min(1000, len(data_loader) - 1)

        lr_scheduler = utils.warmup_lr_scheduler(
            optimizer, warmup_iters, warmup_factor
        )

    for images, targets in metric_logger.log_every(
        data_loader, print_freq, header
    ):
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        loss_dict = model(images, targets)

        losses = sum(loss for loss in loss_dict.values())

        # reduce losses over all GPUs for logging purposes
        loss_dict_reduced = utils.reduce_dict(loss_dict)
        losses_reduced = sum(loss for loss in loss_dict_reduced.values())

        loss_value = losses_reduced.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training.".format(loss_value))
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        losses.backward()
        optimizer.step()

        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced, **loss_dict_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])

    log_dict = {k: v.item() for k, v in loss_dict.items()}
    with open(log_path, "w") as f:
        json.dump(log_dict, f)


def main(args):
    os.makedirs(args.output_dir, exist_ok=True)
    weights_dir = osp.join(args.output_dir, "weights")
    os.makedirs(weights_dir, exist_ok=True)
    log_dir = osp.join(args.output_dir, "log")
    os.makedirs(log_dir, exist_ok=True)

    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    train_dataset = Dataset(
        images_dir=osp.join(args.train_dir, "images"),
        labels_dir=osp.join(args.train_dir, "labels"),
        height=args.height,
        width=args.width,
        transforms=get_transform(is_train=True),
    )

    test_dataset = Dataset(
        images_dir=osp.join(args.test_dir, "images"),
        labels_dir=osp.join(args.test_dir, "labels"),
        height=args.height,
        width=args.width,
        transforms=get_transform(is_train=False),
    )

    print(
        f"""Information

[Dataset]
    - train: {len(train_dataset)}
    - test: {len(test_dataset)}
    - size: ({args.width}, {args.height})

[Network]
    - classes: {args.num_classes}
    - epoch: {args.num_epochs}
    - batch size: {args.batch_size}
    - workers: {args.num_workers}
{'-' * 50}"""
    )

    train_loader = torch.utils.data.DataLoader(
        dataset=train_dataset,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=args.num_workers,
        collate_fn=utils.collate_fn,
    )

    test_loader = torch.utils.data.DataLoader(
        dataset=test_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=args.num_workers,
        collate_fn=utils.collate_fn,
    )

    model = get_object_detection_model(num_classes=args.num_classes)
    model = model.to(device)

    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(
        params, lr=0.005, momentum=0.9, weight_decay=0.0005
    )
    lr_scheduler = torch.optim.lr_scheduler.StepLR(
        optimizer, step_size=5, gamma=0.1
    )

    for epoch in range(args.num_epochs):
        train_one_epoch(
            model,
            optimizer,
            train_loader,
            device,
            epoch,
            args.num_epochs,
            print_freq=1,
            log_path=osp.join(
                log_dir,
                f"loss_epoch_{epoch + 1:>0{len(str(args.num_epochs))}}.json",
            ),
        )
        lr_scheduler.step()

        evaluate(
            model=model,
            data_loader=test_loader,
            device=device,
            log_path=osp.join(
                log_dir,
                f"summary_epoch_{epoch + 1:>0{len(str(args.num_epochs))}}.txt",
            ),
        )

        if (epoch + 1) % 10 == 0:
            torch.save(
                model.state_dict(),
                osp.join(
                    weights_dir,
                    f"epoch_{epoch + 1:>0{len(str(args.num_epochs))}}.weights",
                ),
            )


if __name__ == "__main__":
    cprint("-" * 50, "yellow")
    cprint(("Faster R-CNN Train").center(50), "yellow")
    cprint("-" * 50, "yellow")

    parser = argparse.ArgumentParser(description="Faster R-CNN train")

    # directory settings
    parser.add_argument(
        "-train",
        "--train-dir",
        help="Path to directory for train data",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-test",
        "--test-dir",
        help="Path to directory for test data",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-o", "--output-dir", help="Path to output", type=str, default="output"
    )

    # network settings
    parser.add_argument(
        "--class",
        dest="num_classes",
        help="Number of classes",
        type=int,
        default=2,
    )
    parser.add_argument(
        "--epoch",
        dest="num_epochs",
        help="Number of epochs",
        type=int,
        default=100,
    )
    parser.add_argument(
        "-bs", "--batch-size", help="Batch size", type=int, default=2
    )
    parser.add_argument(
        "-nw", "--num-workers", help="Number of workers", type=int, default=4
    )
    parser.add_argument("--width", help="Image width", type=int, default=320)
    parser.add_argument("--height", help="Image height", type=int, default=240)

    args = parser.parse_args()

    main(args)
